﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Agenda.Models;

namespace Agenda.Dao
{
    public class AgendaContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Address>()
                .HasKey(ad => ad.Id);

            modelBuilder
                .Entity<Contact>()
                .HasRequired(c => c.Address)
                .WithRequiredPrincipal(ad => ad.Contact);

        }
    }
}