﻿namespace Agenda.Models
{
    public class Address
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
        public string AddressComplement { get; set; }
        public TypeAddress TypeAddress { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public virtual Person Person { get; set; }
        public int PersonId { get; set; }
        public virtual Contact Contact { get; set; }
        public int ContactId { get; set; }


    }
}