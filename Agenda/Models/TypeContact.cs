﻿namespace Agenda.Models
{
    public enum TypeContact
    {
        CellPhone, HomePhone, CommercialPhone, Email, Site, Facebook, Twitter
    }
}