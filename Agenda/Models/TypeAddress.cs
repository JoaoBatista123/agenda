﻿namespace Agenda.Models
{
    public enum TypeAddress
    {
        Home, Commercial, Mailbox
    }
}