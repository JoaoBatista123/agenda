﻿namespace Agenda.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public TypeContact TypeContact { get; set; }
        public string Description { get; set; }
        public virtual Address Address { get; set; }
        public int AddressId { get; set; }
        public virtual Person Person { get; set; }
        public int PersonId { get; set; }
        public GroupContact GroupContact { get; set; }

    }
}